(function () {
    'use strict';
    angular.module('ajaxModule', []);
    angular.module('ajaxModule').factory('AjaxFactory', AjaxFactory);

    AjaxFactory.$inject = ['$q', '$http', '$httpParamSerializer'];

    function AjaxFactory($q, $http, $httpParamSerializer) {
        return {
            post: post,
            get: get,
            put: put,
            ajaxDelete: ajaxDelete,
            formEncoded: formEncoded
        }

        function post(url, data, successFunction, errorFunction) {
            $http.post(url, data, {
                'Header': {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (data) {
                    if (!data) {
                        errorFunction(data);
                    }
                    successFunction(data);
                }, function (error) {
                    errorFunction(error);
                });
        }

        function get(url, data, successFunction, errorFunction) {
            $http.get(url, data, {
                'Header': {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (data) {
                    if (!data) {
                        errorFunction(data);
                    }
                    successFunction(data);
                }, function (error) {
                    errorFunction(error);
                });
        }

        function put(url, data, successFunction, errorFunction) {
            $http.put(url, data, {
                'Header': {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (data) {
                    if (!data) {
                        errorFunction(data);
                    }
                    successFunction(data);
                }, function (error) {
                    errorFunction(error);
                });
        }

        function ajaxDelete(url, data, successFunction, errorFunction) {
            $http.delete(url, data, {
                'Header': {
                    'Content-Type': 'application/json'
                }
            })
                .then(function (data) {
                    if (!data) {
                        errorFunction(data);
                    }
                    successFunction(data);
                }, function (error) {
                    errorFunction(error);
                });
        }

        function formEncoded(url, data, successFunction, errorFunction) {
            var req = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $httpParamSerializer(data)
            };
            $http(req)
                .then(function (data) {
                    if (!data) {
                        errorFunction(data);
                    }
                    successFunction(data);
                }, function (error) {
                    errorFunction(error);
                });
        }
    }

})();