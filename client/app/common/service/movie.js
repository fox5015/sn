(function () {
    'use strict';

    angular.module('movieApp.common.service').service('MovieService',movieService);

    movieService.$inject = ['APIPATH','AjaxFactory'];

    function movieService(APIPATH,AjaxFactory){
        this.getMovies = getMovies;
        this.saveRating = saveRating;

        function getMovies(successFunction,errorFunction){
            AjaxFactory.get(APIPATH + 'movies',{}, function(response){
                successFunction(response.data);
            },function(error){
                errorFunction(error);
            })
        }

        function saveRating(data,successFunction,errorFunction){
            AjaxFactory.post(APIPATH + 'movies',data, function(response){
                successFunction(response.data);
            },function(error){
                errorFunction(error);
            })
        }
    }
    
})();