describe('Movie Service', function () {
    var MovieService;

    var movie = [{"$id":"1","title":"Gone Girl","image":"images/movie1.jpg","rating":"3","releaseDate":"2014-02-22"},{"$id":"2","title":"The Good Life","image":"images/movie2.jpg","rating":2,"releaseDate":"2014-06-24"},{"$id":"3","title":"The Hero of Color City","image":"images/movie3.jpg","rating":4,"releaseDate":"2014-11-23"},{"$id":"4","title":"Guardians of the Galaxy","image":"images/movie4.jpg","rating":"5","releaseDate":"2014-07-01"},{"$id":"5","title":"The Drop","image":"images/movie5.jpg","rating":"0","releaseDate":"2014-12-01"},{"$id":"6","title":"If I Stay","image":"images/movie6.jpg","rating":"0","releaseDate":"2018-01-01"}];

    beforeEach(angular.mock.module('movieApp'));

    beforeEach(inject(function (_MovieService_) {
        MovieService = _MovieService_;

        spyOn(MovieService, 'getMovies').and.returnValue(movie);

         spyOn(MovieService, 'saveRating');
         MovieService.saveRating({
             "$id":"1","title":"Gone Girl","image":"images/movie1.jpg","rating":"4","releaseDate":"2014-02-22"
         });

    }));

    it('has a defined', function () {
        // An intentionally failing test. No code within expect() will never equal 4.
        expect(MovieService.getMovies).toBeDefined();
    });

    it('get Movie Data', function () {
        expect(MovieService.getMovies().length).toEqual(6);
    });

    it('save Movie Data', function () {
        expect(MovieService.saveRating).toHaveBeenCalledWith({
             "$id":"1","title":"Gone Girl","image":"images/movie1.jpg","rating":"4","releaseDate":"2014-02-22"
         });
    });
});