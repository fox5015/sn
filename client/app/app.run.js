(function () {
    'use strict';

    angular.module("movieApp").run(run);

    run.$inject = ['$rootScope', '$state'];

    function run($rootScope, $state) {
        $rootScope.$state = $state;
        angular.isUndefinedOrNull = function(value){
            return value === undefined || value === null;
        }

        angular.isEmpty = function(value){
            return value === undefined || value === null || value.length === 0 || value === '' ;
        }
    }

})();