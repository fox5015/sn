(function () {
    'use strict';

    angular.module("movieApp").config(route);

    route.$inject = ['$stateProvider', '$urlRouterProvider'];

    function route($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'home/home.html',
                controller: "HomeController",
                controllerAs: 'home'
            })
             .state('chart', {
                url: '/chart',
                templateUrl: 'chart/chart.html',
                controller: "ChartController",
                controllerAs: 'chart'
            });

        $urlRouterProvider.otherwise('/');
    }

})();