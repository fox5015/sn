describe('HomeController', function(){
    var scope,MovieService;
      var movie = [{"$id":"1","title":"Gone Girl","image":"images/movie1.jpg","rating":"3","releaseDate":"2014-02-22"},{"$id":"2","title":"The Good Life","image":"images/movie2.jpg","rating":2,"releaseDate":"2014-06-24"},{"$id":"3","title":"The Hero of Color City","image":"images/movie3.jpg","rating":4,"releaseDate":"2014-11-23"},{"$id":"4","title":"Guardians of the Galaxy","image":"images/movie4.jpg","rating":"5","releaseDate":"2014-07-01"},{"$id":"5","title":"The Drop","image":"images/movie5.jpg","rating":"0","releaseDate":"2014-12-01"},{"$id":"6","title":"If I Stay","image":"images/movie6.jpg","rating":"0","releaseDate":"2018-01-01"}];

    beforeEach(angular.mock.module('movieApp'));
    beforeEach(angular.mock.inject(function($controller,$rootScope){
        scope = $rootScope.$new();
        MovieService = {
            getMovies: function() {}
        };
        spyOn(MovieService, 'getMovies').and.returnValue(movie);

        $controller('HomeController as home', {$scope: scope ,MovieService : MovieService});


    }));

    it('should have title ', function(){
        expect(scope.home.title).toBe("Home Controller");
    });

    it('should have rating allowed for today relase movie ', function(){
         var tempDate = new Date();
        tempDate.setDate(tempDate.getDate());
        expect(scope.home.ratingAllowed({ releaseDate: tempDate } )).toBeTruthy();
    });

}); 