(function () {
    'use strict';

    angular.module("movieApp").controller('HomeController',homeController);

    homeController.$inject = ['$filter', 'MovieService'];

    function homeController($filter,MovieService) {
        var vm= this;
        
        vm.title = 'Home Controller';

        function init(){
            MovieService.getMovies(function(data){
                vm.movieData = data;
            },function(error){
                console.log(error);
            })
        }

        vm.ratingAllowed = function(item){
            return new Date(item.releaseDate) <= new Date();
        };

        vm.releaseDateFilter = function(item){
            return $filter('date')(new Date(item.releaseDate),'dd-MMM-yy');
        };

        vm.saveRating = function(item){
            MovieService.saveRating(item,function(data){
            },function(error){
            });
        }

        init();

    }

})();