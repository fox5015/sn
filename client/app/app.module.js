(function () {
  'use strict';

  angular.module('movieApp', [
    'ajaxModule',
    'movieApp.core',
    'movieApp.common'
  ]);
})();