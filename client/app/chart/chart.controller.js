(function () {
    'use strict';

    angular.module("movieApp").controller('ChartController', chartController);

    chartController.$inject = ['$filter', 'MovieService'];

    function chartController($filter, MovieService) {
        var vm = this;

        function setChartData() {
            vm.chartLabels = [];
            vm.chartSeries = ['Movie'];

            vm.chartData = [];
            var tempData = [];
            for (var i = 0; i <= 5; i++) {
                vm.chartLabels.push(i);
                var temp = $filter('filter')(vm.movieData, {
                    'rating': i
                });
                tempData.push(temp.length);
            }
            vm.chartData.push(tempData);
        }

        function getMovieData() {
            MovieService.getMovies(function (data) {
                vm.movieData = data || [];
                setChartData();
            }, function (error) {
                console.log(error);
            })
        }

        function init() {
            getMovieData();
        }

        init();

    }

})();