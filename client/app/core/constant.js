(function () {
    'use strict';

    angular.module('movieApp.core')
        .constant('APIPATH', 'http://localhost:3000/api/');
})();