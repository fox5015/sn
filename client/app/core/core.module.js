angular.module('movieApp.core', ['ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ngSanitize',
    'ngStorage',
    'chart.js'
]);