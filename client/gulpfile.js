// gulp
var gulp = require('gulp');

var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var cleanCss = require('gulp-clean-css');
var rev = require('gulp-rev');
var del = require('del');
var gulpSequence = require('gulp-sequence');

// plugins
var connect = require('gulp-connect');

gulp.task('connect', function () {
  connect.server({
    root: __dirname + '/app',
    port: 8887
  });
});


gulp.task('minified', function () {
  return gulp.src('app/index.html')
    .pipe(usemin({
      css: [rev],
      html: [htmlmin({
        collapseWhitespace: true
      })],
      js: [uglify, rev]
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('clean', function () {
  return del('build', {
    force: true
  });
});

gulp.task('copy-html-files', function () {
  gulp.src(['./app/**/*.html', '!./app/bower_components/**/*.html'])
    .pipe(gulp.dest('build/'));
});

gulp.task('copy-image-files', function () {
  gulp.src(['./app/assets/images/**'])
    .pipe(gulp.dest('build/assets/images'));
});

gulp.task('connectprod', function () {
  connect.server({
    root:  __dirname + '/build',
    port: 9999
  });
});

gulp.task('copy-bootstrap-fonts', function () {
  return gulp.src('!./app/bower_components/bootstrap/fonts/**.*')
    .pipe(gulp.dest('build/fonts'));
});


gulp.task('default', ['connect']);


gulp.task('build', function (cb) {
  gulpSequence( 'clean', 'copy-html-files','copy-bootstrap-fonts', 'copy-image-files', 'minified','connectprod')(cb)
})