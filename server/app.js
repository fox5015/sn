'use strict';

var dotenv = require('dotenv').config();
var config = require('./config/config');
var _ = require('lodash');
var glob = require('glob');



// Init the express application
var app = require('./config/express')();

// Start the app by listening on <port>
app.listen(config.port);

// Expose app
exports = module.exports = app;

// Logging initialization
console.log('MovieRating application started on port ' + config.port);