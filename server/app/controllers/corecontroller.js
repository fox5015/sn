'use strict';

var path = require('path'),
    _ = require('lodash'),
    fs = require('fs');

exports.index = function (req, res) {
    console.log(req.useragent);
    res.json({
        message: 'Not found'
    });
};


exports.getMovies = function (req, res) {
    fs.readFile('app/data/movies.json', 'utf8', function (err, data) {
        if (err) throw err;
        var obj = JSON.parse(data);
        res.json(obj);
    });
}


exports.saveMovie = function (req, res) {
    var data = req.body;
    var obj = require('../data/movies.json');
    var index = _.findIndex(obj, {
        '$id': data.$id
    });
    obj[index] = data;
    fs.writeFile('app/data/movies.json', JSON.stringify(obj), function (err) {
        if (err) throw err;
        res.json(data);
    });
}