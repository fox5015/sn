'use strict';

module.exports = function (app) {
    // Root routing
    var core = require('../../app/controllers/corecontroller');
    app.route('/').get(core.index);

    app.route('/api/movies').get(core.getMovies);
    app.route('/api/movies').post(core.saveMovie);
};